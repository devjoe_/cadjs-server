const Payload = require('../lib/payload');
const {send, events} = require('../managers/socketManager');

class identify extends Payload {
    constructor() {
        super('identify');
        this.requiresLogin = false;
    }
    execute(connection, pd) {
        if(pd === undefined) return send(connection, events.INVALID_PAYLOAD);
        if(pd.email === undefined) return send(connection, events.INVALID_PAYLOAD);
        if(pd.password === undefined) return send(connection, events.INVALID_PAYLOAD);
        return send(connection, events.NOT_IMPLEMENTED);
    }
}

module.exports = identify;