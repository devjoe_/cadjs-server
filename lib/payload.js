class Payload {
    constructor(name) {
        this.name = name.toUpperCase();
        this.requiresLogin = true;
    }

    execute(connection, pd) {

    }
}

module.exports = Payload;