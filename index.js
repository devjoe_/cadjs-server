const globals = require('./globals');
const sqlManager = require('./managers/sqlManager');
const prepSQL = require('./sql');
const express = require('express');
const config = require('./config.json');
const {handleNewConnection, loadPayloads} = require('./managers/socketManager');

sqlManager.connect().catch((e) => console.error(e)); // Connect to Postgres

prepSQL(); // Run table and schema creations

let app = express();

app.use((require('morgan'))('combined'));

const expressws = require('express-ws')(app);

app.ws('/ws', handleNewConnection);

loadPayloads() // Load client-executable payloads

app.listen(config.server_port, () => console.log("LISTEN: Listening on "+config.server_port));