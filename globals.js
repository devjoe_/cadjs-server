const globals = {};

module.exports.get = (name) => {
    return globals[name];
}

module.exports.set = (name, value) => {
    globals[name] = value;
}

module.exports.delete = (name) => {
    delete globals[name];
}