const { Pool, Client } = require('pg');
const globals = require('../globals');
const config = require('../config.json');

module.exports.connect = () => {
    return new Promise((res, rej) => {
        globals.set('pg.pool', new Pool({
            user: config.postgres.user,
            host: config.postgres.host,
            database: config.postgres.database,
            password: config.postgres.password,
            port: config.postgres.port
        }));
        globals.get('pg.pool').query('SELECT NOW()', (err, re) => {
            if(err) return rej(err);
            return res(re);
        });
    });
}

module.exports.shutdown = () => {
    globals.get('pg.pool').end();
    globals.delete('pg.pool');
}