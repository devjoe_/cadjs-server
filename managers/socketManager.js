const globals = require('../globals');
const fs = require('fs');
const path = require('path');
const snowflake = require('node-snowflake').Snowflake;
const config = require('../config.json');

let connections = {};
let payloads = {};
globals.set('express.connections', connections);
globals.set('express.payloads', payloads);

const events = {
    "HELLO": "HELLO",
    "INVALID_PAYLOAD": "INVALID_PAYLOAD",
    "INVALID_PAYLOAD_NAME": "INVALID_PAYLOAD_NAME",
    "NOT_AUTHORIZED": "NOT_AUTHORIZED",
    "NOT_IMPLEMENTED": "NOT_IMPLEMENTED"
}

module.exports.events = events;

module.exports.loadPayloads = () => {
    let dir = fs.readdirSync(path.join(__dirname, '../payloads'));
    let ps = dir.filter((e) => e.endsWith('.js'));
    console.log("DISCOVER: Discovered "+ps.length+" payloads.");
    ps.forEach((e) => {
        try {
            let payload = new (require('../payloads/'+e))();
            payloads[payload.name] = payload;
            console.log("DISCOVER: Loaded "+e+" / "+payload.name);
        } catch(e) {
            console.error("INVALID PAYLOAD: "+e);
            console.error(e.stack);
        }
    });
    
}

function send(connection, event, data) {
    let out = {event: event.toUpperCase(), data: data, _connectionID: connection.id};
    console.log("OUT: "+JSON.stringify(out));
    connection.ws.send(JSON.stringify(out));
}

module.exports.send = send;

module.exports.handleNewConnection = (ws, req) => {
    let connID = snowflake.nextId();
    ws.on('message', (msg) => {
        // Check to make sure the payload matches specifications
        console.log("IN: "+msg);
        let json = null;
        try {
            json = JSON.parse(msg);
        } catch(e) {
            ws.close();
            return;
        }
        if(json.payload === undefined) {
            ws.close();
            return;
        }

        // Decide & execute payload
        let payload = payloads[json.payload];
        if(payload === undefined) {
            send(connections[connID], events.INVALID_PAYLOAD_NAME);
        } else {
            if(payload.requiresLogin && connections[connID].user === null) return send(connections[connID], events.NOT_AUTHORIZED);
            payload.execute(connections[connID], json.data);
        }
    });

    ws.on('close', () => {
        if(connections[connID] !== undefined) {
            delete connections[connID];
            console.log("CLEARED: Cleared one connection.");
        }
        console.log("DISC!")
    });

    connections[connID] = {
        ws: ws,
        user: null, // Will be populated by login payload
        req: req,
        id: connID
    }
    console.log("EST!");
    send(connections[connID], "hello", {heartbeatInterval: config.heartbeat_interval});
};