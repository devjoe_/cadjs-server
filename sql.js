const globals = require('./globals');

function execC(pool, c) {
    return new Promise((res, rej) => {
        pool.query(c, (e, r) => {
            if(e) return rej(e);
            return res();
        });
    });
}

module.exports = () => {
    let pool = globals.get('pg.pool');
    execC(pool, 'CREATE SCHEMA IF NOT EXISTS openjs_data').then(() => {
        execC(pool, 'CREATE TABLE IF NOT EXISTS openjs_data.users ( id integer NOT NULL, email character varying NOT NULL, password character varying NOT NULL, callsigns json, characters json, is_admin int NOT NULL, CONSTRAINT users_pkey PRIMARY KEY (id) ) WITH (OIDS=FALSE);');

    }).catch((e) => { console.error(e); });
}